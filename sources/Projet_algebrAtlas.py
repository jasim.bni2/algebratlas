import time
import tkinter as tk
import tkinter.ttk as ttk
from random import randint

import customtkinter
import matplotlib.pyplot as plt
import numpy as np
import pygame
from customtkinter import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from sympy import *
from sympy.abc import x

pygame.mixer.init()

init_printing(use_unicode=True)
x, y, z = symbols('x y z')

chemin_audio = "action_music.mp3"
musique_fond = pygame.mixer.Sound(chemin_audio)

# Nombre total de questions
nombre_questions = 7

# Liste pour stocker les réponses du joueur et les réponses correctes
reponses_joueur = []
reponses_correctes = []

# Liste pour stocker les questions posées
questions_posees = []

# Temps total
temps_total = 0
# Temps de début de la partie
temps_debut_partie = 0

# Variable pour vérifier si le joueur a répondu à au moins une question
repondu_question = False

# Variable pour vérifier si la musique de fond est activée
musique_fond_activee = False


def relancer_jeu():
    global reponses_joueur, reponses_correctes, questions_posees, temps_total, temps_debut_partie, repondu_question

    # Réinitialisation des listes de réponses et de questions
    reponses_joueur = []
    reponses_correctes = []
    questions_posees = []

    # Réinitialisation des variables de temps
    temps_total = 0
    temps_debut_partie = 0

    repondu_question = False

    # Affichage du bouton pour commencer le jeu
    bouton_commencer.pack()


def generer_question():
    # Coefficients aléatoires
    a = randint(1, 5)
    b = randint(1, 5)
    c = randint(1, 5)

    # Création fonction aléatoire
    fonction = a * x ** 2 + b * x + c

    # Calcul de la dérivée
    derivee = diff(fonction, x)

    return fonction, derivee


# vérifie la validité de la réponse de l'utilisateur
def verifier_reponse(reponse_utilisateur):
    # Vérification de la validité de la réponse
    if reponse_utilisateur.strip() == "":  # vérifie si la réponse de l'utilisateur est vide
        return False
    try:
        sympify(reponse_utilisateur)
        if 'x' not in reponse_utilisateur:
            if any(char.isdigit() for char in reponse_utilisateur):
                return True
            return False
        elif any(char.isalpha() and char != 'x' for char in reponse_utilisateur):
            return False
        return True
    except SympifyError:
        return False


# Fonction pour soumettre la réponse
def soumettre_reponse(event=None):
    global temps_total, temps_debut_partie, repondu_question
    repondu_question = True
    temps_reponse = time.time() - temps_debut_question
    temps_total += temps_reponse
    reponse = entree_reponse.get()
    if verifier_reponse(reponse):
        fonction_actuelle, derivee_actuelle = questions_posees[-1]
        reponses_joueur.append(reponse)
        reponses_correctes.append(str(derivee_actuelle))
        poser_question()
    entree_reponse.delete(0, "end")


# Fonction pour passer la question
def passer_question():
    global temps_total
    poser_question()


# Fonction pour poser une nouvelle question
def poser_question():
    global temps_debut_question
    temps_debut_question = time.time()
    if len(questions_posees) < nombre_questions:
        fonction, derivee = generer_question()
        questions_posees.append((fonction, derivee))
        etiquette_question.configure(text="Question " + str(len(questions_posees)) + "/" + str(nombre_questions) +
                                          ": Quelle est la dérivée de la fonction : f(x) = " + str(fonction) + " ?")
        entree_reponse.pack(pady=5)
        bouton_soumettre.pack(pady=5)
        bouton_passer.pack(pady=5)
    else:
        afficher_score()


def afficher_score():
    global repondu_question
    fenetre_tableau_scores = tk.Toplevel()
    fenetre_tableau_scores.title("Tableau des scores")

    if not repondu_question:
        message = "Vous n'avez répondu à aucune question ! Peut-être la prochaine fois !"
        ttk.Label(fenetre_tableau_scores, text=message).pack()
    else:
        tableau_scores = ttk.Treeview(fenetre_tableau_scores, columns=(
            "Question", "Réponse de l'utilisateur", "Réponse correcte"), show="headings")
        tableau_scores.heading("Question", text="Question")
        tableau_scores.heading("Réponse de l'utilisateur", text="Réponse de l'utilisateur")
        tableau_scores.heading("Réponse correcte", text="Réponse correcte")

        for i in range(len(reponses_correctes)):
            question = i + 1
            reponse_utilisateur = reponses_joueur[i] if i < len(reponses_joueur) else "Aucune réponse"
            reponse_correcte = reponses_correctes[i]
            tableau_scores.insert("", "end", values=(question, reponse_utilisateur, reponse_correcte))

        tableau_scores.pack(fill="both", expand=True)

        message_appreciation = "Merci d'avoir participé !"

        ttk.Label(fenetre_tableau_scores, text=message_appreciation).pack()

    # Afficher toutes les réponses à chaque question
    for i in range(len(reponses_correctes)):
        question = i + 1
        reponse_utilisateur = reponses_joueur[i] if i < len(reponses_joueur) else "Aucune réponse"
        reponse_correcte = reponses_correctes[i]
        ttk.Label(fenetre_tableau_scores, text=f"Question {question}: Votre réponse: {reponse_utilisateur}, Réponse correcte: {reponse_correcte}").pack()

    # Appel de la fonction pour relancer le jeu après avoir affiché le score
    relancer_jeu()



# Fonction pour commencer le jeu
def commencer_jeu():
    global temps_debut_partie, repondu_question, temps_total
    temps_debut_partie = time.time()
    repondu_question = False
    temps_total = 0
    bouton_commencer.pack_forget()  # masque le widget
    poser_question()


# Fonction pour activer ou désactiver la musique de fond
def gerer_musique_fond():
    global musique_fond_activee
    if musique_fond_activee:
        musique_fond.stop()
        musique_fond_activee = False
        bouton_musique_fond.configure(text="Activer musique de fond")
    else:
        musique_fond.play()
        musique_fond_activee = True
        bouton_musique_fond.configure(text="Désactiver musique de fond")

def jouer_musique_fond():
    musique_fond.play()
    fenetre.after(2000, verifier_musique_fond)

def verifier_musique_fond():
    if not musique_fond_activee:
        return
    if not pygame.mixer.get_busy():
        jouer_musique_fond()


def derive(nombre):
    global resultat
    resultat = diff(nombre, x)
    return resultat


def clic_sur_bouton():
    print("que voulez vous faire?")


def affiche_derive():
    label_resultat.configure(text=str(resultat))


def slider_event(value):
    print(value)


def plot_equation(expression1, expression2):
    plt.style.use('dark_background')

    # Définition de l'intervalle de valeurs de x
    x_min = -1000
    x_max = 1000
    x_values = np.linspace(x_min, x_max, 100)

    # Évaluation des expressions pour obtenir les valeurs de y correspondantes
    y1 = [eval(expression1, {'x': i}) for i in x_values]
    y2 = [eval(expression2, {'x': i}) for i in x_values]

    fig, ax = plt.subplots()
    ax.plot(x_values, y1, label='Equation 1')
    ax.plot(x_values, y2, label='Equation 2')
    ax.set_xlabel('X-axis')
    ax.set_ylabel('Y-axis')

    label_x_pos = x_values[50]  # Position x au milieu de l'intervalle
    label_y1_pos = y1[60]  # Position y pour y1 au milieu de l'intervalle
    label_y2_pos = y2[30]  # Position y pour y2 au milieu de l'intervalle

    equation_text1 = r'$y_1 = {}$'.format(expression1)
    equation_text2 = r'$y_2 = {}$'.format(expression2)
    ax.text(label_x_pos, label_y1_pos, equation_text1, fontsize=12, color='turquoise',
            ha='center')  # Texte pour y1 en rouge
    ax.text(label_x_pos, label_y2_pos, equation_text2, fontsize=12, color='khaki', ha='center')  # Texte pour y2 en bleu

    ax.set_title('Graph with Equations')
    ax.legend()

    canvas = FigureCanvasTkAgg(fig, master=tab3)
    canvas.draw()
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)


def recupere_equation_derive():
    global equation
    equation = entry.get()
    derive(equation)
    affiche_derive()
    for c in tab3.winfo_children():
        c.destroy()
    plot_equation(str(equation), str(resultat))
    bouton_graphique.place(x=220, y=240)
    my_tab.select(tab3)


def afficher_position(evt):
    pos_x, pos_y = evt.x, evt.y
    affichage = f"Position : abscisse = {pos_x} ; ordonnées = {pos_y}"
    return pos_x, pos_y


"""def change_legende () : 
   global bouton1 
   bouton1.config(text = "le résultat est ")"""


def efface_texte():
    entry.delete(0, END)


def clavier(event):
    touche = event.keysym
    global equation
    equation = entry.get()
    derive(equation)
    affiche_derive()
    for c in tab3.winfo_children():
        c.destroy()
    plot_equation(str(equation), str(resultat))
    bouton_graphique.place(x=220, y=240)
    my_tab.select(tab3)


def voir_le_graphique():
    my_tab.show(tab3)  # Sélectionne l'onglet contenant le graphique
    bouton_graphique.place(x=220, y=240)  # Déplacer le bouton sur l'onglet 3


def calcul():
    calcul = sortie.get('0.0', 'end')
    resultat = eval(calcul)
    sortie.delete('0.0', 'end')
    sortie.insert('0.0', resultat)


def resoud_calcul(event):
    touche = event.keysym
    calcul = sortie.get('0.0', 'end')
    resultat = eval(calcul)
    sortie.delete('0.0', 'end')
    sortie.insert('0.0', resultat)


def changer_theme(event=None):
    # Récupérer la sélection de l'utilisateur
    mode_theme = choix_theme.get()

    # Mettre à jour le thème en fonction du choix de l'utilisateur
    if mode_theme == "Sombre":
        customtkinter.set_appearance_mode("dark")
    elif mode_theme == "Clair":
        customtkinter.set_appearance_mode("light")
    else:
        # Si le mode est système, utilisez le thème système par défaut
        customtkinter.set_appearance_mode("system")


def creer_onglet_parametres():
    global choix_theme
    # Ajouter des widgets pour sélectionner le mode de thème
    etiquette_mode_theme = customtkinter.CTkLabel(tab6, text="Choisir le mode de thème :", font=("Helvetica", 20))
    etiquette_mode_theme.pack(pady=10)

    choix_theme = ttk.Combobox(tab6, values=["Sombre", "Clair", "Système"])
    choix_theme.bind("<<ComboboxSelected>>", changer_theme)  # Lier la sélection à la fonction de changement de thème
    choix_theme.pack(pady=5)


fenetre = customtkinter.CTk()

fenetre.geometry("650x600")

customtkinter.set_appearance_mode("dark")
customtkinter.set_default_color_theme("dark-blue")

my_tab = customtkinter.CTkTabview(fenetre, width=650, height=600)
my_tab.pack(fill="both", expand=True, padx=10, pady=10)

tab1 = my_tab.add("Accueil")
tab2 = my_tab.add("Dérivation")
tab3 = my_tab.add("Graphique")
tab4 = my_tab.add("Calculatrice")
tab5 = my_tab.add("Jeu")
tab6 = my_tab.add("Paramètres")

bouton_commencer = customtkinter.CTkButton(tab5, text="Commencer", font=("Helvetica", 14), command=commencer_jeu)
bouton_commencer.pack(pady=50)

# Bouton pour activer ou désactiver la musique de fond
bouton_musique_fond = customtkinter.CTkButton(tab5, text="Activer musique de fond", font=("Helvetica", 14),
                                              command=gerer_musique_fond)
bouton_musique_fond.pack(pady=10)

# Étiquette pour la question
etiquette_question = customtkinter.CTkLabel(tab5, text="", font=("Helvetica", 14))
etiquette_question.pack(pady=20)

# Entrée pour la réponse
entree_reponse = customtkinter.CTkEntry(tab5, font=("Helvetica", 14))
entree_reponse.bind("<Return>", soumettre_reponse)  # Lier la touche "Entrée" à la soumission de réponse

# Bouton pour soumettre la réponse
bouton_soumettre = customtkinter.CTkButton(tab5, text="Soumettre", font=("Helvetica", 14), command=soumettre_reponse)

# Bouton pour passer la question
bouton_passer = customtkinter.CTkButton(tab5, text="Passer", font=("Helvetica", 14), command=passer_question)

# Temps de début de la question
temps_debut_question = 0

"""photo = PhotoImage(file='logo_projet_nsi.PNG') 
label_image = Label(tab1,bg="moccasin", image=photo) 
label_image.place(x=0, y=0)"""

canvas = tk.Canvas(tab1, width=650, height=600, highlightthickness=0)
canvas.pack()

img = tk.PhotoImage(file="logo_projet_nsi.PNG")
canvas.create_image(322, 300, image=img)

# draw text
creer_onglet_parametres()

"""texte = "Bienvenue sur Dérive.com - Jasim et Aymane" 
label_texte = Label(tab1, text=texte, font=("Helvetica", 14)) 
label_texte.place(x=150, y=150)"""

entry = customtkinter.CTkEntry(master=tab2, placeholder_text="Entrer la fonction", width=200, height=30,
                               corner_radius=10)
entry.place(x=220, y=50)

label_texte1 = customtkinter.CTkLabel(tab2, width=50, height=30, text="f(x) =", font=("Helvetica", 24))
label_texte1.place(x=150, y=50)

bouton_recupere = customtkinter.CTkButton(master=tab2, text="Dériver la fonction", width=200, height=30,
                                          corner_radius=50, command=recupere_equation_derive)
bouton_recupere.place(x=170, y=110)

slider = customtkinter.CTkSlider(master=tab2,
                                 width=160,
                                 height=16,
                                 border_width=5.5,
                                 command=slider_event)
slider.place(x=300, y=300)

"""bouton1 = tk.Button(tab1, text="Résoudre", command=clic_sur_bouton, font=("cambria math", 15)) 
bouton1.place(x=280, y=250, height=20)"""

label_texte1 = customtkinter.CTkLabel(tab2, width=50, height=30, text="f'(x) =", font=("Helvetica", 24))
label_texte1.place(x=150, y=200)

label_resultat = customtkinter.CTkLabel(tab2, text="", font=("cambria math", 24))
label_resultat.place(x=215, y=147)

bouton_graphique = customtkinter.CTkButton(tab2, text="Voir le graphique", font=("Helvetica", 14),
                                           command=voir_le_graphique)

"""bouton1.config(command = change_legende)"""
bouton_efface = customtkinter.CTkButton(tab2, text="Effacer", corner_radius=50, font=("Helvetica", 14),
                                        command=efface_texte)
bouton_efface.place(x=400, y=110)

sortie = customtkinter.CTkTextbox(tab4, width=340, height=30, corner_radius=20, border_width=5, border_color='#042940',
                                  font=(('Arial', 50)))
sortie.grid(row=0, column=0, columnspan=5, padx=5, pady=5)

# Définition des boutons dans une liste pour simplifier

liste_bouton = [['1', '2', '3', '+'], ['4', '5', '6', '-'], ['7', '8', '9', 'x'], ['0', 'C', '=', '/']]

# Création des boutons avec une double boucle

for i in range(len(liste_bouton) - 1):
    for j in range(len(liste_bouton[i]) - 1):
        btn = customtkinter.CTkButton(tab4, text=liste_bouton[i][j], corner_radius=20, width=80, height=55,
                                      font=(('Helvetica', 30)),
                                      command=lambda i=i, j=j: sortie.insert('end', liste_bouton[i][j]))
        btn.grid(row=i + 1, column=j, padx=5, pady=5)

# lambda i=i, j=jcapture les valeurs actuelles de i et j pour chaque bouton
btn0 = customtkinter.CTkButton(tab4, text='0', corner_radius=20, width=80, height=55, font=(('Helvetica', 30)),
                               command=lambda: sortie.insert('end', 0))
btn0.grid(row=4, column=0, padx=0, pady=5)

btn_effacer = customtkinter.CTkButton(tab4, text='C', corner_radius=20, width=80, height=55, font=(('Helvetica', 30)),
                                      command=lambda: sortie.delete('0.0', 'end'))
btn_effacer.grid(row=4, column=1, padx=0, pady=5)

btn_calculer = customtkinter.CTkButton(tab4, text='=', corner_radius=20, width=80, height=55, font=(('Helvetica', 30)),
                                       command=calcul)
btn_calculer.grid(row=4, column=2, padx=0, pady=5)

btn_additionner = customtkinter.CTkButton(tab4, text='+', corner_radius=20, width=80, height=55,
                                          font=(('Helvetica', 30)), command=lambda: sortie.insert('end', '+'))
btn_additionner.grid(row=1, column=3, padx=0, pady=5)

btn_soustraire = customtkinter.CTkButton(tab4, text='-', corner_radius=20, width=80, height=55,
                                         font=(('Helvetica', 30)), command=lambda: sortie.insert('end', '-'))
btn_soustraire.grid(row=2, column=3, padx=0, pady=5)

btn_multiplier = customtkinter.CTkButton(tab4, text='x', corner_radius=20, width=80, height=55,
                                         font=(('Helvetica', 30)), command=lambda: sortie.insert('end', '*'))
btn_multiplier.grid(row=3, column=3, padx=0, pady=5)

btn_diviser = customtkinter.CTkButton(tab4, text='/', corner_radius=20, width=80, height=55, font=(('Helvetica', 30)),
                                      command=lambda: sortie.insert('end', '/'))
btn_diviser.grid(row=4, column=3, padx=0, pady=5)

entry.bind("<Return> ", clavier)
sortie.bind("<Return>", resoud_calcul)
bouton_graphique.bind("<Return>", voir_le_graphique)

fenetre.mainloop()